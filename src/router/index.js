
//Guidelines: 1) Import the component. 2) Export to a route

import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import App from '@/App'
// import TopSection from '@/components/TopSection'

////////////////////////

Vue.use(Router)

/////////////////////////

export default new Router({
  routes: [
    {
      path: '/',
      name: 'App',
      component: App
     }
  ]
})
