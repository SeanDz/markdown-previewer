// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import TopSection from './components/TopSection.vue'
import WriteArea from './components/WriteArea.vue'
import MarkdownPreviewer from './components/MarkdownPreviewer.vue'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App,
    TopSection,
    WriteArea,
    MarkdownPreviewer
  }
})
